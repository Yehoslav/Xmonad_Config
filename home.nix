{
  config,
  lib,
  pkgs,
  ...
}: {
  xsession.windowManager = {
    xmonad = {
      enable = true;
      enableContribAndExtras = true;
      extraPackages = hpkgs: [
        hpkgs.xmonad
        hpkgs.dbus
        hpkgs.xmonad-contrib
        hpkgs.xmonad-extras
      ];

      config = ./xmonad.hs;

      libFiles = {
        "MyKeys.hs" = ./lib/MyKeys.hs;
        "MyVars.hs" = ./lib/MyVars.hs;
        "MyProjects.hs" = ./lib/MyProjects.hs;
      };
    };
  };

  home.file.".xmonad/scripts" = {
    source = ./scripts;
    recursive = true;
  };
}
