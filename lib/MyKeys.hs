{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

module MyKeys where

import System.Exit
import System.IO

import XMonad
import XMonad.Layout.Gaps
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.ResizableTile (MirrorResize (MirrorExpand, MirrorShrink))
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts

import XMonad.Util.Dmenu
import XMonad.Util.EZConfig (mkKeymap)
import XMonad.Util.NamedScratchpad
import XMonad.Util.Replace ()
import XMonad.Util.Run (safeSpawnProg, spawnPipe)

import XMonad.Actions.CopyWindow
import XMonad.Actions.CycleWS (nextScreen)
import XMonad.Actions.DynamicProjects
import XMonad.Actions.FindEmptyWorkspace
import XMonad.Actions.FloatKeys (keysMoveWindow, keysResizeWindow)
import XMonad.Actions.Submap

import XMonad.Hooks.Modal

import qualified XMonad.StackSet as W

import MyProjects as P
import MyVars

-- Key Modes

myModes :: [Mode]
myModes = [noModMode, myFloatMode 20, gapsMode, resizeMode]

myFloatModeLabel, gapsModeLabel, resizeModeLabel :: String
gapsMode, resizeMode :: Mode
myFloatMode ::
    -- | Step size
    Int ->
    Mode

myFloatModeLabel = "MyFloat"

myFloatMode s =
    mode myFloatModeLabel $
        mkKeysEz
            [ -- move
              ("h", withFocused (keysMoveWindow (-s, 0)))
            , ("t", withFocused (keysMoveWindow (0, s)))
            , ("n", withFocused (keysMoveWindow (0, -s)))
            , ("s", withFocused (keysMoveWindow (s, 0)))
            , -- enlarge
              ("M-h", withFocused (keysResizeWindow (s, 0) (1, 0)))
            , ("M-t", withFocused (keysResizeWindow (0, s) (0, 0)))
            , ("M-n", withFocused (keysResizeWindow (0, s) (0, 1)))
            , ("M-s", withFocused (keysResizeWindow (s, 0) (0, 0)))
            , -- shrink
              ("M-S-h", withFocused (keysResizeWindow (-s, 0) (0, 0)))
            , ("M-S-t", withFocused (keysResizeWindow (0, -s) (0, 1)))
            , ("M-S-n", withFocused (keysResizeWindow (0, -s) (0, 0)))
            , ("M-S-s", withFocused (keysResizeWindow (-s, 0) (1, 0)))
            ]

gapsModeLabel = "Gaps"
gapsMode =
    mode gapsModeLabel $
        mkKeysEz
            [ ("n", resizeGaps IncGap) -- Increase all gaps
            , ("t", resizeGaps DecGap) -- Decrease all gaps
            , ("i", incWindowSpacing 3) -- Increase spacing
            , ("d", decWindowSpacing 3) -- Decrease spacing
            , ("t", sendMessage ToggleGaps) -- Toggle gaps
            ]
  where
    resizeGaps action = do
        sendMessage $ action 6 U
        sendMessage $ action 6 D
        sendMessage $ action 6 R
        sendMessage $ action 6 L

resizeModeLabel = "Resize"
resizeMode =
    mode resizeModeLabel $
        mkKeysEz
            [ ("h", sendMessage Shrink) -- Shrink
            , ("s", sendMessage Expand) -- Expand
            , ("n", sendMessage MirrorExpand) -- Mirror Expand
            , ("t", sendMessage MirrorShrink) -- Mirror Shrink
            ]

-- Key Chords

keyMapDoc :: String -> X Handle
keyMapDoc n = do
    -- focused screen location/size
    r <-
        withWindowSet $
            return . screenRect . W.screenDetail . W.current

    spawnPipe $
        unwords
            [ "~/.xmonad/scripts/showHintForKeymap.sh"
            , n
            , show (rect_x r)
            , show (rect_y r)
            , show (rect_width r)
            , show (rect_height r)
            , "purple" -- key color
            , "white" -- cmd color
            , "Input\\ Mono" -- font
            , "18" -- line height
            ]

toSubmap :: XConfig l -> String -> [(String, X ())] -> X ()
toSubmap c n m = do
    pipe <- keyMapDoc n
    submap $ mkKeymap c m
    io $ hClose pipe

openKeymap =
    -- Open
    [ ("b", spawn "librewolf") -- Browser (librewolf)
    , ("e", spawn "emacs") -- Emacs
    , ("f", spawn "thunar") -- Gui File Manager
    , ("h", inTerm "btop") -- Btop
    , ("l", inTerm "lf") -- lf terminal file manager
    , ("v", spawn "virt-manager") -- Virtual Manager
    , ("q", spawn "qutebrowser") -- Qutebrowser
    ]

musicKeymap =
    [ ("r", inTerm "ncmpcpp") -- run ncmpcpp
    ]

configKeymap =
    -- Most important config files
    [ ("x", switchProject xmonadConfig) -- XMonad config
    , ("h", switchProject nixosConfig) -- NixOS config
    , ("s", spawn "copyq show") -- Show copyq window
    , ("c", spawn (myExtendHome "~/.local/bin/dm-clip")) -- Select from copyq with dmenu
    ]

focusKeymap =
    -- Focus
    [ ("b", focusW "librewolf") -- browser
    , ("e", focusW "emacs") -- Emacs
    , ("m", windows W.focusMaster) -- Focus Master
    , ("/", spawn windowSelectionMenu)
    ]
  where
    focusW :: String -> X ()
    focusW w = spawn ("wmctrl -a " ++ w)
    windowSelectionMenu = "wmctrl -l | cut -d' ' -f 5- | sort | uniq -u | dmenu -i -c -l 8 | xargs -IWIN wmctrl -F -a WIN"

generalKeymap =
    -- General stuff
    [ ("s", withFocused $ windows . W.sink) -- Sink window
    , ("S-r", spawn "xmonad --recompile && xmonad --restart") -- Restart
    , ("b", sendMessage $ Toggle NOBORDERS) -- Toggle borders
    , ("n", nextScreen)
    ]

masterKeymap =
    -- Master Window
    [ ("f", windows W.focusMaster) -- Focus
    , ("s", windows W.swapMaster) -- Swap
    , ("k", sendMessage (IncMasterN 1)) -- Inc
    , ("j", sendMessage (IncMasterN (-1))) -- Dec
    ]

shotKeymap =
    -- Screen Shot
    [ ("c", takeShot fullScreen) -- Full screen
    , ("s", takeShot select) -- Select region
    , ("w", takeShot currentWindow) -- Current Window
    , ("C-c", copyShot fullScreen) -- Copy full screen
    , ("C-s", copyShot select) -- Copy select region
    , ("C-w", copyShot currentWindow) -- Copy current Window
    , ("S-c", editShot fullScreen) -- Edit full screen
    , ("S-s", editShot select) -- Edit select region
    , ("S-w", editShot currentWindow) -- Edit current Window
    , ("g", editGIMP select) -- Edit selection in GIMP
    , ("o", openDirectory) -- Open screenshot folder
    ]
  where
    takeShot m = spawn (scrot m $ myExtendHome "~/Pictures/screenshots/'%Y-%m-%dT%H%M%S_$wx$h.png'")
    copyShot m = spawn (scrot m "-" |> xclip "-i")
    editShot m = spawn (scrot m "-" |> xclip "-i" &> "drawing -c")
    editGIMP m = spawn ("rm -f " ++ tmpFile &> scrot m tmpFile &> "gimp " ++ tmpFile)

    openDirectory = spawn (myTerminal ++ " -e lf " ++ myExtendHome "/Pictures/screenshots/")
    select = " -a $(slop --highlight --color=0.3,0.4,0.6,0.4 -f '%x,%y,%w,%h')"
    currentWindow = "-u"
    fullScreen = " "

    tmpFile = "/tmp/edit-with-gimp.png"
    scrot o p = "scrot " ++ o ++ " " ++ p
    xclip opt = "xclip " ++ opt ++ " -sel clip -t image/png"
    lft |> rgt = lft ++ " | " ++ rgt
    lft &> rgt = lft ++ " && " ++ rgt

scratchpadKeymap =
    -- Scratchapds
    [ ("k", scr (name keepass)) -- Keepass Scractchpad
    , ("t", scr (name telegram)) -- Telegram Scractchpad
    , ("d", scr (name konsole)) -- Dropdown konsole
    , ("u", scr (name tuigram)) -- Tuigram
    , ("a", scr (name alacritty)) -- Dropdown alacritty
    ]
  where
    scr = namedScratchpadAction scratchpads

vKeymap =
    -- Video related
    [ ("r", spawn $ myExtendHome "~/.local/bin/ffrecord.clj") -- Video record
    , ("v", spawn "mpv \"$(xclip -o -sel clip)\"") -- Open the url with mpv
    , ("y", spawn $ xclip "-o" |> "sed -E 's/youtu(be\\.com|\\.be)/yewtu.be/'" |> xclip "-i")
    ]
  where
    xclip opt = "xclip " ++ opt ++ " -sel clip "
    lft |> rgt = lft ++ " | " ++ rgt

projectsKeymap =
    -- Project quick access
    [ ("a", switchProject P.arcaLuiNoe) -- Arca lui Noe
    , ("h", switchProject P.orarium) -- Orarium
    , ("p", switchProject (Project "prg" "~/Development/" Nothing)) -- Programming workspace
    , ("n", switchProject (Project "net" "~/" Nothing)) -- Internet stuff
    , ("i", switchProject (Project "init" "~/" Nothing)) -- Init ws
    , ("u", switchProject (Project "uni" "~/Uni/" Nothing)) -- University stuff
    , ("g", switchProject (Project "grf" "~/" Nothing)) -- Graphical editing
    , ("v", switchProject (Project "vid" "~/Videos/" Nothing)) -- Video editing
    , ("o", switchProject (Project "org" "~/" Nothing)) -- Organisation and Emacs
    ]

dmenuKeymap =
    -- Dmenu actions
    [ ("r", spawn "dmenu_run -i -c -l 15") -- Dmenu run
    , ("e", spawn $ myExtendHome "~/.local/bin/dm-emoji") -- Get Emoji
    , ("p", spawn $ myExtendHome "~/.local/bin/dm-pass") -- Get Password
    , ("s", spawn $ myExtendHome "~/.local/bin/search.bb") -- Search prompt
    , ("h", spawn "cat ~/.local/share/yehos/html_symbols | sed 's/|/  /g' | dmenu -l 10 -c | awk -F'  ' '{print $1}' | xclip -i -sel cli") -- Html symbols
    ]

bookmarksKeymap =
    -- Bookmarks
    [ ("g", spawn $ myExtendHome "~/.local/bin/bookmark -g") -- Get bookmarks
    , ("s", spawn $ myExtendHome "~/.local/bin/bookmark -s") -- Save a bookmark from clipboard
    ]

mainKeymap c =
    mkKeymap
        c
        $ [ -- Window Manager Controls
            ("M-<Return>", safeSpawnProg myTerminal) -- Terminal
          , ("M-C-S-q", io exitSuccess) -- Exit
          , ("M-y", windows copyToAll) -- Copy focused window to all ws
          , ("M-S-c", kill) -- Kill current window
          , ("M-M1-c", killAllOtherCopies) -- Kill the copies of the focused window
          , ("M-S-<Space>", sendMessage NextLayout) -- Next Layout
          , ("M-.", windows W.focusDown) -- Next Window
          , ("M-,", windows W.focusUp) -- Prev Window
          , ("M-S-y", spawn "polybar-msg cmd toggle") -- Toggle Polybar
          , ("M-f", sendMessage $ Toggle NBFULL) -- Toggle full screen
          , ("M-e", viewEmptyWorkspace)
          , ("M-S-e", tagToEmptyWorkspace)
          , ("M-M1-w", wsAction "Altul:" altWs)
          , ("M-S-w", shiftToWs)
          , ("M-C-w", wsAction "Change to:" changeWsWithShift)
          , ("M-C-h", sendMessage $ pullGroup L)
          , ("M-C-s", sendMessage $ pullGroup R)
          , ("M-C-n", sendMessage $ pullGroup U)
          , ("M-C-t", sendMessage $ pullGroup D)
          , ("M-C-m", withFocused (sendMessage . MergeAll))
          , ("M-C-u", withFocused (sendMessage . UnMerge))
          , ("M-<Tab>", onGroup W.focusUp')
          , ("M-S-<Tab>", onGroup W.focusDown')
          , ("M-S-a", withFocused $ toggleDynamicNSP "dyn1")
          , ("M-a", dynamicNSPAction "dyn1")
          ]
            ++ [ -- INFO: Key Chords and modes
                 ("M-o", toSubmap c "openKeymap" openKeymap) -- Open
               , ("M-m", toSubmap c "masterKeymap" masterKeymap) -- Master
               , ("M-S-m", toSubmap c "musicKeymap" musicKeymap) -- Music
               , ("M-c", toSubmap c "configKeymap" configKeymap) -- Configs
               , ("M-g", toSubmap c "generalKeymap" generalKeymap) -- General
               , ("M-S-f", toSubmap c "focusKeymap" focusKeymap) -- Focus
               , ("M-S-/", toSubmap c "mainKeymap" []) -- Toggle this help screen
               , ("M-l", toSubmap c "scratchpadKeymap" scratchpadKeymap) -- Scratchapds
               , ("M-v", toSubmap c "vKeymap" vKeymap) -- [V] maps (usualy [v]ideos)
               , ("M-d", toSubmap c "dmenuKeymap" dmenuKeymap) -- Dmenu actions
               , ("M-b", toSubmap c "bookmarksKeymap" bookmarksKeymap) -- bookmarks actions
               , ("M-<Print>", toSubmap c "shotKeymap" shotKeymap) -- Screen Shot
               , ("M-w", toSubmap c "projectsKeymap" projectsKeymap) -- Volume
               , ("M-S-n", setMode noModModeLabel)
               , ("M-S-g", setMode gapsModeLabel)
               , ("M-S-r", setMode myFloatModeLabel)
               , ("M-r", setMode resizeModeLabel)
               ]
            ++ [ -- Media keys
                 ("<XF86MonBrightnessUp>", spawn "brightnessctl s +1%")
               , ("<XF86MonBrightnessDown>", spawn "brightnessctl s 1%-")
               , ("<XF86AudioLowerVolume>", spawn "amixer sset Master 10%-")
               , ("<XF86AudioRaiseVolume>", spawn "amixer sset Master 10%+")
               , ("<XF86AudioMute>", spawn "amixer sset Master toggle")
               ]
  where
    allWs = do
        ws <- map W.tag <$> gets (W.workspaces . windowset)
        let aws = ws ++ [projectName p | p <- projects]
        return aws

    dmenuArgs prompt = menuArgs "dmenu" ["-c", "-p", prompt, "-l", "5", "-g", "3"]

    changeWsWithShift maybePrj prjName =
        case maybePrj of
            Just prj -> do
                shiftToProject prj
                switchProject prj
            Nothing -> do
                shiftToProject (Project prjName "~/" Nothing)
                switchProject (Project prjName "~/" Nothing)

    altWs maybePrj prjName =
        case maybePrj of
            Just prj -> switchProject prj
            Nothing -> switchProject (Project prjName "~/" Nothing)

    shiftToWs = do
        prjName <- allWs >>= \ws -> dmenuArgs "Shift to:" ws
        windows (W.shift prjName)

    wsAction actPrompt act = do
        prjName <- allWs >>= \ws -> dmenuArgs actPrompt ws
        if prjName == ""
            then return ()
            else lookupProject prjName >>= \prj -> act prj prjName
