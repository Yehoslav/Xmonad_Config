module MyProjects where

import XMonad
import XMonad.Actions.DynamicProjects

import MyVars

projects :: [Project]
projects = [orarium, arcaLuiNoe, xmonadConfig, nixosConfig, ludus]

nixosConfig, xmonadConfig, arcaLuiNoe, orarium, ludus :: Project
nixosConfig =
    Project
        { projectName = "nixos-config"
        , projectDirectory = "~/.setup"
        , projectStartHook = Just $ do spawn "st"
        }
xmonadConfig =
    Project
        { projectName = "xmonad-config"
        , projectDirectory = myExtendHome "~/Development/Xmonad_Config/"
        , projectStartHook = Just $ do spawn "st -e nix-shell"
        }
ludus =
    Project
        { projectName = "ludus"
        , projectDirectory = myExtendHome "~/Development/Ludus/"
        , projectStartHook = Just $ do
            spawn "st"
            spawn "st -e nix develop"
        }
arcaLuiNoe =
    Project
        { projectName = "arca-lui-noe"
        , projectDirectory = "~/Development/arcaluinoe.info/"
        , projectStartHook = Just $ do spawn "st"
        }
orarium =
    Project
        { projectName = "orarium"
        , projectDirectory = myExtendHome "~/Development/Orarium"
        , projectStartHook = Just $
            do
                spawn "st -e nix-shell --command \"lein repl\""
        }
