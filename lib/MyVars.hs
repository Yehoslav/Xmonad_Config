module MyVars where

import XMonad

import qualified XMonad.StackSet as W
import XMonad.Util.NamedScratchpad

myTerminal :: String
myTerminal = "st"

myEditor :: String
myEditor = "nvim"

myGuiEditor :: String
myGuiEditor = "neovide"

-- FIXME
myExtendHome :: String -> String
myExtendHome ('~' : xs) = "/home/yehoslav" ++ xs
myExtendHome s = s

-- neorgInbox :: String
-- neorgInbox = "$HOME/Documents/neorg/gtd/inbox.norg"

-- Scratchapds

telegram, keepass, alacritty, konsole, tuigram :: NamedScratchpad

scratchpads :: [NamedScratchpad]
scratchpads = [telegram, keepass, alacritty, konsole, tuigram]

telegram = NS "telegram" "telegram-desktop" (className =? "TelegramDesktop") tallFloat
  where
    tallFloat = customFloating $ W.RationalRect (2 / 5) (1 / 5) (2 / 5) (3 / 5)

tuigram = NS "st" tuigramCmd (className =? "Tuigram") centerFloat
  where
    centerFloat = customFloating $ W.RationalRect (1 / 10) (1 / 10) (3 / 5) (3 / 5)
    tuigramCmd = "st  -c Tuigram -e tuigram"

keepass = NS "keepass" "keepassxc" (className =? "KeePassXC") defaultFloating

alacritty = NS "alacritty" alacrittyCmd (className =? "alacritty-dropdown") centerFloat
  where
    centerFloat = customFloating $ W.RationalRect (1 / 5) (1 / 5) (3 / 5) (3 / 5)
    alacrittyCmd = "alacritty --class 'alacritty-dropdown'"

konsole = NS "konsole" konsoleCmd (appName =? "konsole-dropdown") centerFloat
  where
    konsoleCmd = "konsole --name 'konsole-dropdown'"
    centerFloat = customFloating $ W.RationalRect (1 / 5) (1 / 5) (3 / 5) (3 / 5)

inTerm :: String -> X ()
inTerm comand = spawn (myTerminal ++ " -e " ++ comand)

