#!/usr/bin/env bash

function run {
  if ! pgrep "$1" ;
  then
    "$@"&
  fi
}

xsetroot -cursor_name left_ptr &
run mcron -d /home/yehoslavr/.config/cron
wal -R -t

#starting utility applications at boot time
run nm-applet &
run volumeicon &
numlockx on &
blueberry-tray &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
run dunst &

#starting user applications at boot time
run polybar &
run syncthing &
run copyq &
