{pkgs ? import <nixpkgs> {}}:
with pkgs;
  mkShell {
    buildInputs = [
      (ghc.withPackages (p: [
        p.cabal-install
        p.ormolu
        p.implicit-hie
        p.X11
        p.dbus
      ]))

      haskellPackages.hoogle
      haskellPackages.haskell-language-server
      haskellPackages.hlint
      haskellPackages.ghcid
      haskellPackages.fourmolu

      pkg-config
      expat

      xorg.libX11
      xorg.libX11.dev

      xorg.libXft
      xorg.libXext
      xorg.libXrandr
      xorg.libXrender
      xorg.libXinerama
      xorg.libXScrnSaver
      xorg.libXdmcp
    ];
  }
