{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

import Control.Monad (liftM2)
import qualified DBus as D
import qualified DBus.Client as D
import qualified Data.ByteString as B
import qualified Data.Map as M

import XMonad
import XMonad.Config.Desktop

import XMonad.Actions.DynamicProjects
import XMonad.Actions.MouseResize
import XMonad.Actions.SpawnOn
import XMonad.Actions.WindowNavigation
import XMonad.Actions.TiledWindowDragging

import XMonad.Hooks.Modal
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers (doCenterFloat, isDialog)
import XMonad.Hooks.SetWMName
import XMonad.Hooks.WindowSwallowing

import XMonad.Layout.BoringWindows
import XMonad.Layout.Dishes
import XMonad.Layout.DraggingVisualizer
import XMonad.Layout.Master
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.ResizableTile
import XMonad.Layout.Spacing
import XMonad.Layout.Spiral (spiral)
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowArranger
import XMonad.Layout.WindowNavigation

import qualified XMonad.StackSet as W

import XMonad.Util.EZConfig (additionalKeys)
import XMonad.Util.Replace

import XMonad.Util.NamedScratchpad

import MyKeys
import MyProjects as P
import MyVars

myStartupHook :: X ()
myStartupHook = do
    spawn $ myExtendHome "~/.xmonad/scripts/autostart.sh"
    setWMName "LG3D"

-- myTerminal = "alacritty"

myModMask :: KeyMask
myModMask = mod4Mask

encodeCChar = map fromIntegral . B.unpack

myWorkspaces :: [String]
myWorkspaces = ["init", "prg", "org", "uni", "net", "grf"]

myBaseConfig = desktopConfig

-- window manipulations
myManageHook =
    composeAll . concat $
        [ [isDialog --> doCenterFloat]
        , [className =? c --> doCenterFloat | c <- myCFloats]
        , [title =? t --> doFloat | t <- myTFloats]
        , [resource =? r --> doFloat | r <- myRFloats]
        , [resource =? i --> doIgnore | i <- myIgnores]
        , [ app `shiftTo` workspace
          | (shifts, workspace) <- zip myShifts myWorkspaces
          , app <- shifts
          ]
        ]
  where
    doShiftAndGo = doF . liftM2 (.) W.greedyView W.shift
    shiftTo x wks = (className =? x <||> title =? x <||> resource =? x) --> doShiftAndGo wks
    myCFloats = ["feh"] -- Class floats
    myTFloats = ["Media viewer", "Downloads", "Save As..."] -- Title floats
    myRFloats = []
    myIgnores = ["desktop_window"]
    my1Shifts = []
    my2Shifts = ["Emacs"]
    my3Shifts = ["FreeCAD", "OpenSCAD"]
    my4Shifts = ["brave-browser"]
    my5Shifts = ["Gimp", "Inkscape", "com.github.maoschanz.drawing"]
    myShifts = [my1Shifts, my2Shifts, my3Shifts, my4Shifts, my5Shifts]

myLayout =
    draggingVisualizer $
        spacingRaw True (Border 5 5 5 5) True (Border 5 5 5 5) True $
            avoidStruts $
                mouseResize $
                    windowArrange $
                        mkToggle (NBFULL ?? NOBORDERS ?? EOT) $
                            windowNavigation $
                                subTabbed
                                    ( boringWindows $
                                        tiled
                                            ||| spiral (6 / 7)
                                            ||| mastered (1 / 100) (2 / 5) (Dishes 1 (1 / 6))
                                    )
  where
    tiled = ResizableTall 1 delta tiled_ratio []
    delta = 3 / 100
    tiled_ratio = 1 / 2

myMouseBindings (XConfig{XMonad.modMask = modMask_}) =
    M.fromList
        -- mod-button1, Set the window to floating mode and move by dragging
        [ ((modMask_, 1), \w -> focus w >> mouseMoveWindow w >> windows W.shiftMaster)
        , -- mod-button2, Raise the window to the top of the stack
          ((modMask_, 2), \w -> focus w >> windows W.shiftMaster)
        , -- mod-button3, Set the window to floating mode and resize by dragging
          ((modMask_, 3), \w -> focus w >> mouseResizeWindow w >> windows W.shiftMaster)
        , ((modMask_ .|. shiftMask, 1), dragWindow)
        ]

main :: IO ()
main = do
    dbus <- D.connectSession

    -- Request access to the DBus name
    _ <-
        D.requestName
            dbus
            (D.busName_ "org.xmonad.Log")
            [D.nameAllowReplacement, D.nameReplaceExisting, D.nameDoNotQueue]

    replace
    myConfig <- withWindowNavigation (xK_n, xK_h, xK_t, xK_s) defaults
    xmonad . ewmh . modal myModes $
        dynamicProjects P.projects $
            myConfig
                `additionalKeys` ( [ ((m .|. myModMask, k), windows $ f w)
                                   | (w, k) <- zip myWorkspaces [xK_1 .. xK_9]
                                   , (f, m) <-
                                        [ (W.greedyView, 0)
                                        , (W.shift, shiftMask)
                                        , (\i -> W.greedyView i . W.shift i, shiftMask)
                                        ]
                                   ]
                                    ++ [ ((m .|. myModMask, k), windows $ f i)
                                       | (i, k) <- zip myWorkspaces [xK_1 .. xK_9]
                                       , (f, m) <- [(W.greedyView, 0), (W.shift, controlMask)]
                                       ]
                                 )

myHandleEventHook = swallowEventHook (className =? "St") (return True)

defaults =
    XMonad.def
        { startupHook = myStartupHook
        , layoutHook = myLayout ||| layoutHook myBaseConfig
        , manageHook = manageSpawn <+> myManageHook <+> manageHook myBaseConfig <+> namedScratchpadManageHook scratchpads
        , modMask = myModMask
        , borderWidth = 2
        , handleEventHook = myHandleEventHook <+> handleEventHook myBaseConfig
        , focusFollowsMouse = True
        , workspaces = myWorkspaces
        , focusedBorderColor = "#af3a03"
        , normalBorderColor = "#928374"
        , keys = mainKeymap
        , mouseBindings = myMouseBindings
        }
